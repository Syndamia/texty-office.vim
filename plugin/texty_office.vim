if exists('g:loaded_texty_office') || &cp
	  finish
endif

let g:loaded_texty_office = '0.1.0' " version number
" Guard against flags
let s:oldcpo = &cpo
set cpo&vim

if !exists('g:texty_office_executable_directory')
  let g:texty_office_executable_directory = ''
endif

if !exists('g:texty_office_pretty_mode')
	let g:texty_office_pretty_mode = 1
endif

" zip plugin will automatically unzip those files, so we must stop it
if !exists('g:zipPlugin_ext')
	" This isn't ideal, since if any more file types are added, this will need to
	" be updated too, but there haven't been any changes to zipPlugin_ext from
	" 2015, so it should be fine
	let g:zipPlugin_ext= '*.zip,*.jar,*.xpi,*.ja,*.war,*.ear,*.celzip,
	  \ *.oxt,*.kmz,*.wsz,*.xap,*.docm,*.dotx,*.dotm,*.potx,*.potm,
	  \ *.ppsx,*.ppsm,*.pptm,*.ppam,*.sldx,*.thmx,*.xlam,*.xlsm,
	  \ *.xlsb,*.xltx,*.xltm,*.xlam,*.crtx,*.vdw,*.glox,*.gcsx,*.gqsx,*.epub'
else
	let g:zipPlugin_ext=substitute(g:zipPlugin_ext, '\*\.\(xlsx\|docx\|pptx\).\?', '', 'g')
endif

augroup texty_office_rw
	au!
	autocmd BufReadCmd *.xlsx,*.docx,*.pptx call texty_office#ReadDocument(expand('<afile>:p'))
	autocmd BufWriteCmd *.xlsx,*.docx,*.pptx call texty_office#WriteDocument(expand('<afile>:p'))
augroup END

let &cpo = s:oldcpo
unlet s:oldcpo
