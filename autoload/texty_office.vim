function! texty_office#ReadDocument(filename)
	let executable = s:FindExec()
	if executable == ''
		echoerr 'Couldn''t find command-line executable ''texty-office''!'
		return
	endif
	if !filereadable(a:filename)
		echoerr 'File doesn''t exist: '..a:filename
		return
	endif

	let text = system(executable..' -o '..shellescape(a:filename))
	if v:shell_error
		echoerr 'Error while executing ''texty-office'': '..text
		return
	endif

	call clearmatches()
	let text = split(text, '\%x00')

	if g:texty_office_pretty_mode == 1
		let &filetype=matchstr(a:filename, '\.\zs[^.]*$')

		let i = 0
		while i < len(text)
			let text[i] = substitute(text[i], '\\href{\([^{}]*\)}{\([^{}]*\)}', '\\href{\2<\1>}', 'g')
			let text[i] = substitute(text[i], '\\item', '■', 'g')
			let text[i] = s:MatchAddTex(text[i], i+1, 'sout', 'officeSout')
			let text[i] = s:MatchAddTex(text[i], i+1, 'textit', 'officeItal')
			let text[i] = s:MatchAddTex(text[i], i+1, 'textul', 'officeUl')
			let text[i] = s:MatchAddTex(text[i], i+1, 'textbf', 'officeBold')
			let text[i] = s:MatchAddTex(text[i], i+1, 'section', 'officeSection')
			let text[i] = s:MatchAddTex(text[i], i+1, 'href', 'officeHref')
			let i += 1
		endwhile
	endif

	set modifiable
	call append(0, text)
	$delete _
	call cursor(1, 1)

	set readonly " Writing to file isn't yet implemented
	if g:texty_office_pretty_mode == 1
		set nomodifiable " Highlighting cannot be tracked
	else
		set filetype=tex
	endif
endfunction

function! s:MatchAddTex(line, ln, name, group)
	let line = a:line
	let matched = matchstrpos(line, '\\'..a:name..'{[^}]*}')
	while matched[1] > -1
		let rmLen = len(a:name)+3

		let hima = getmatches()
		let i = -1
		while i+1 < len(hima)
			let i += 1
			" Refer to :h matchaddpos()
			let endpos = hima[i]['pos1'][1]+hima[i]['pos1'][2]-1
			if (hima[i]['pos1'][0] != a:ln) || (endpos < matched[1]+1)
				continue
			endif

			" Handles: "... matched ... hima ..."
			if matched[2] < hima[i]['pos1'][1]
				let hima[i]['pos1'][1] -= rmLen

			" Handles: "... hima_start ... matched ... hima_end ..."
			elseif matched[2] <= endpos
				let hima[i]['pos1'][2] -= rmLen

			" Handles: "... hima_start ... match_start ... hima_end ... match_end ..."
			" This is relevant, because for a TeX expression, the ending curly
			" brace is the first closing curly brace, so in "\a{\b{...}}", the
			" first highlight (for \a) is done on the text "\a{\b{...}", but
			" when we want to also highlight \b (and fix length of highlight
			" of \a), we have "hima_start \a{\b{... hima_end }" and for \b
			" our match will be "\b{... hima_end }"
			else
				let hima[i]['pos1'][2] -= min([rmLen-1, endpos - matched[1] + 1])
			endif
		endwhile
		call setmatches(hima)

		" In this case, we would have had line[:-1], which is the entire line
		if matched[1] == 0
			let line = matched[0][rmLen-1:-2]..line[matched[2]:]
		else
			let line = line[:matched[1]-1]..matched[0][rmLen-1:-2]..line[matched[2]:]
		endif
		call matchaddpos(a:group, [[a:ln, (matched[1]+1), (len(matched[0]) - rmLen)]])
		let matched = matchstrpos(line, '\\'..a:name..'{[^}]\+}')
	endwhile
	return line
endfunction

function! texty_office#WriteDocument(filename)
	echoerr 'Writing to file isn''t implemented yet!'
endfunction

function! s:FindExec()
	if g:texty_office_executable_directory != ''
		let local_exec = g:texty_office_executable_directory..'/texty-office'
		if !isabsolutepath(g:texty_office_executable_directory)
			let local_exec = findfile(local_exec, escape(&runtimepath, ' '))
		endif

		if local_exec != '' && filereadable(local_exec)
			return local_exec
		endif
	endif

	if executable('texty-office')
		return 'texty-office'
	else
		return ''
	endif
endfunction
