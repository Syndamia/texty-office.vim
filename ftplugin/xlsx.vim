hi officeSout cterm=strikethrough gui=strikethrough
hi officeItal cterm=italic gui=italic
hi officeUl cterm=underline gui=underline
hi officeBold cterm=bold gui=bold
hi link officeSection Title
hi officeHref ctermfg=Magenta guifg=Magenta
