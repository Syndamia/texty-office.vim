# texty-office.vim

A vim "front-end" for [texty-office](https://gitlab.com/Syndamia/texty-office).
Just open a DOCX, XLSX or PPTX file as any normal file, and you'll see it's content in text form.
There are some sample documents in the aforementioned texty-office.

For more information, refer to the [documentation](./doc/texty_office.txt).
